package server

import (
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"os"
	"io/ioutil"
	"path/filepath"
	"archive/zip"
	"bytes"
)

type Server struct {
	files  []*ServedNode
	router *mux.Router
}

func ServerNew() *Server {
	this := new(Server)
	this.router = mux.NewRouter()
	this.router.HandleFunc("/files/{file}", this.filesHandler)

	this.router.HandleFunc("/assets/{type}/{asset}", this.assetHandler)

	this.router.HandleFunc("/", this.fileListHandler)
	this.router.HandleFunc("/zip/", this.zipHandler)
	this.router.HandleFunc("/download/download.zip", this.downloadZipHandler)

	http.Handle("/",this.router)
	return this
}

func (this *Server) AddDir(file string, info os.FileInfo, err error) error {
	if err != nil {
		panic(err)
	}
	if !info.IsDir() {
		this.AddFile(file)
	}

	return nil
}

func (this *Server) RemoveDir(file string, info os.FileInfo, err error) error {
	if err != nil {
		panic(err)
	}
	if !info.IsDir() {
		this.RemoveFile(file)
	}

	return nil
}

//
// Adds a file to the served file collection
//
func (this *Server) AddFile(uri string) {
	file, err := ServedNodeNew(uri)
	if err != nil {
		panic(err)
	}
	fileinfo , _ := os.Stat(file.Uri)
	if  fileinfo.IsDir() {
		filepath.Walk(file.Uri, this.AddDir)
	} 
	this.files = append(this.files, file)

}

// removes a file from the file array
func (this *Server) RemoveFile(uri string) {
	for idx, file := range this.files {
		if file.Uri == uri {
			fileinfo , _ := os.Stat(file.Uri)
			if  fileinfo.IsDir() {
				filepath.Walk(file.Uri, this.RemoveDir)
			} else{
				this.files[idx] = this.files[len(this.files)-1]
				this.files = this.files[0 : len(this.files)-1]
			}
		}
	}
}

func (this *Server) Serve() {
	//make inform parent routine if we crash
	defer func(){
		if err := recover(); err != nil {
			log.Println(err)
		}
	}()

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err) //server crashed 
	}
}

func (this *Server) fileListHandler(w http.ResponseWriter, r *http.Request) {

	t, err:= template.ParseFiles("tpl/index.html", "tpl/files.html")

	if err != nil{
		panic(err)
	}

	err = t.ExecuteTemplate(w, "base", this.files)

	if err != nil{
		log.Println(err)
	}


}

func (this *Server) filesHandler(w http.ResponseWriter, r *http.Request) {
	filename := mux.Vars(r)["file"]

	log.Println(filename)
	for _, file := range this.files {
		if file.Name == filename {
			http.ServeFile(w, r, file.Uri)
		}
	}

}

func (this *Server) assetHandler(w http.ResponseWriter, r *http.Request){
	params := mux.Vars(r)
	log.Println(params)

	assetpath, err :=filepath.Abs("public/" + params["type"] +"/" + params["asset"]) 
	if err !=nil {
		log.Println(err)
	}
	http.ServeFile(w, r, assetpath )


	
}

func (this *Server) zipHandler(w http.ResponseWriter, r *http.Request){
	r.ParseForm()
	formData := r.Form
	
	//initialize zip buffer
	buf := new(bytes.Buffer)
	zipwriter := zip.NewWriter(buf)

	for _, val := range formData {
		for _, file := range this.files {
			if file.Name == val[0] {
				zipfile, err := zipwriter.Create(file.Name)
				
				if err != nil{
					log.Fatal(err)
				}
				rawFile,_ := ioutil.ReadFile(file.Uri)
				
				_, err = zipfile.Write([]byte(rawFile))
			
			}
		}
	}
	
	zipwriter.Close()   
	ioutil.WriteFile("public/download/download.zip", buf.Bytes(), 0777)    

	


}

func (this *Server) downloadZipHandler(w http.ResponseWriter, r *http.Request){

	http.ServeFile(w, r, "public/download/download.zip")
	
}
