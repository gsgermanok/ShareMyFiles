package ui

import (
	"github.com/ggerman/ShareMyFiles/lib/server"
	"github.com/conformal/gotk3/gtk"
	"log"
)

type MainWindow struct {
	Window    *gtk.Window
	HeaderBar *gtk.HeaderBar

	MainArea *gtk.Box
	FileList *gtk.ListBox

	myServer *server.Server

	/* HEADER AREA WIDGETS*/
	RunServerButton   *gtk.Button
	AddFileButton     *gtk.Button
	AddFileLabel      *gtk.Label
	OptionsMenuButton *gtk.MenuButton
	OptionsMenu       *gtk.Menu
	Options           []*gtk.MenuItem
	err               error
}

func MainWindowNew() *MainWindow {
	this := new(MainWindow)
	this.Window, this.err = gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	this.FileList, this.err = gtk.ListBoxNew()
	if this.err != nil {
		log.Fatal("unable to create window:", this.err)
	}

	this.myServer = server.ServerNew()

	go this.myServer.Serve()

	this.MainArea, this.err = gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 2)

	this.InitializeHeaderBar()
	this.Window.Add(this.FileList)

	this.Window.Connect("destroy", func() {

		gtk.MainQuit()
	})

	this.Window.SetSizeRequest(800, 600)

	return this
}

func (this *MainWindow) InitializeHeaderBar() {
	this.HeaderBar, this.err = gtk.HeaderBarNew()
	this.OptionsMenuButton, this.err = gtk.MenuButtonNew()
	this.AddFileLabel, this.err = gtk.LabelNew("Add File")
	this.OptionsMenu, this.err = gtk.MenuNew()
	this.AddFileButton, this.err = gtk.ButtonNewFromIconName("document-open", gtk.ICON_SIZE_SMALL_TOOLBAR)

	//	var item *gtk.MenuItem, err error = gtk.MenuItemNewWithLabel("Add File")
	//	this.Options = append(this.Options, item)
	this.HeaderBar.PackStart(this.AddFileButton)
	this.HeaderBar.PackStart(this.AddFileLabel)

	this.HeaderBar.PackEnd(this.OptionsMenuButton)

	this.OptionsMenuButton.SetPopup(this.OptionsMenu)

	this.AddFileButton.Connect("clicked", this.AddFileDialog)

	if this.err != nil {
		panic(this.err)
	}

	this.HeaderBar.SetTitle("ShareMyFiles")
	this.HeaderBar.SetShowCloseButton(true)
	this.Window.SetTitlebar(this.HeaderBar)

}

func (this *MainWindow) AddFileDialog() {
	dialog, err := gtk.DialogNew()
	filechooser, err := gtk.FileChooserWidgetNew(gtk.FILE_CHOOSER_ACTION_OPEN)
	dialog.AddButton("Share", gtk.RESPONSE_ACCEPT)
	dialog.AddButton("Cancel", gtk.RESPONSE_CANCEL)
	dialog.SetTitle("Share files")
	box, err := dialog.GetContentArea()
	box.Add(filechooser)
	box.ShowAll()
	switch dialog.Run() {
	case int(gtk.RESPONSE_ACCEPT):
		this.myServer.AddFile(filechooser.GetFilename())

		delete, err := gtk.ButtonNewWithLabel("Delete")
		rowBox, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 1)
		content, err := gtk.EntryNew()
		delete.SetRelief(gtk.RELIEF_NONE)
		rowBox.Add(content)
		rowBox.Add(delete)
		content.SetText(filechooser.GetFilename())
		delete.Connect("clicked", func(){this.RemoveFile(content)})
		rowBox.SetChildPacking(content, true, true, 0, gtk.PACK_START)
		rowBox.SetHomogeneous(false)

		if err != nil {
			panic(err)
		}

		this.FileList.Insert(rowBox, -1)
		dialog.Destroy()
		this.Window.ShowAll()
		break
	case int(gtk.RESPONSE_CANCEL):
		dialog.Destroy()
		return
	}

	if err != nil {
		panic(err)
	}

}

func (this *MainWindow) RemoveFile(fileEntry * gtk.Entry) {
	filename, err := fileEntry.GetText()
	box, err := fileEntry.GetParent()
	if err != nil { // box destroyed prematurely
		panic(err)
	}
	this.myServer.RemoveFile(filename)

	box.Destroy()

}
